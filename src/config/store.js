import {configureStore} from '@reduxjs/toolkit'
import todoSlice from './todoSlice'

const rootReducer={
    todos:todoSlice
}
const store=configureStore({
    reducer:rootReducer
   
})

export default store
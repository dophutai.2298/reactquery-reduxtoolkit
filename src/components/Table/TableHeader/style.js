import { makeStyles } from '@material-ui/core/styles'
import colors from '../../../constants/colors'
const useStyles = makeStyles((theme) => ({
    td: {
        padding: '8px 16px',
        fontSize: '12px',
        fontWeight: 700,
        textTransform: 'uppercase',
        color: colors.gray4,
        width: 'inherit',
        minWidth: 'inherit'
    }
}))

export default useStyles
import React, { memo } from 'react'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Box from '@material-ui/core/Box'
import { arrayOf, bool, func, number, oneOf, shape, string } from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSort, faSortDown, faSortUp } from '@fortawesome/free-solid-svg-icons'
import colors from '../../../constants/colors'
import  {autoGenId}  from '../../../constants/common'
import { ORDER } from '../enums'
import useStyles from './style'

const sortableDisplay = ({ id, orderBy, order }) => {
  const isActive = orderBy === id
  const color = isActive ? colors.cobalt : colors.gray4
  const icon = isActive ? (order === ORDER.ASC ? faSortUp : faSortDown) : faSort
  return (
    <Box pl={0.5}>
      <FontAwesomeIcon size="sm" color={color} icon={icon} />
    </Box>
  )
}
const TableHeader = ({ onRequestSort, order, orderBy, headerCells }) => {
  const classes = useStyles()
  const handleOnSortClick = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <thead >
      <tr >
        {headerCells.map((headerCell) => {
          return (
            <td
              className={classes.td}
              key={autoGenId(headerCell.id, 'table-header_cell')}
              id={autoGenId(headerCell.id, 'table-header_cell')}
            >
              {headerCell.sortable ? (
                <TableSortLabel
                  style={{ color: colors.gray4 }}
                  id={autoGenId(headerCell.id, 'table-header_cell_sortable')}
                  IconComponent={() => null}
                  active={true}
                  onClick={handleOnSortClick(headerCell.id)}>
                  {headerCell.label}
                  {sortableDisplay({ id: headerCell.id, orderBy, order })}
                </TableSortLabel>
              ) : (
                headerCell.label
              )}
            </td>
          )
        })}
      </tr>
    </thead>
  )
}

TableHeader.propTypes = {
  order: oneOf([ORDER.ASC, ORDER.DESC]),
  orderBy: string,
  classes: string,
  headerCells: arrayOf(
    shape({
      id: string,
      numeric: bool,
      disablePadding: bool,
      label: string,
      sortable: bool,
      component: func,
      minWidth: number,
    })
  ),
  onRequestSort: func,
}

TableHeader.defaultProps = {
  order: ORDER.ASC,
  orderBy: '',
  classes: '',
  headerCells: [],
  onRequestSort: () => { },
}

const checkChangeProps = (prevProps, nextProps) => {
  return (
    prevProps.orderBy === nextProps.orderBy &&
    prevProps.order === nextProps.order
  )
}

export default memo(TableHeader, checkChangeProps)
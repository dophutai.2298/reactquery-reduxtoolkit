
import React, { memo, useLayoutEffect, useState } from 'react'  
import MuiTable from '@material-ui/core/Table'
import MuiTableContainer from '@material-ui/core/TableContainer'
import Paper from '@material-ui/core/Paper'
import CircularProgress from '@material-ui/core/CircularProgress'
import { arrayOf, bool, func, object, oneOf, shape, string } from 'prop-types'
import { Typography } from '@material-ui/core'
import isEqual from 'lodash/isEqual'
import TableHeader from './TableHeader'
import { ORDER } from './enums'
import { stableSort } from './utils'
import { autoGenId, uuidv4 } from '../../constants/common'
import useStyles from './style'

const Table = (props) => {
  const { headerCells, items, defaultSort, defaultSortBy, columnId, loading } = props
  const [order, setOrder] = useState(defaultSort)
  const [rows, setRows] = useState(items)
  const [orderBy, setOrderBy] = useState(defaultSortBy)
  const classes = useStyles()

  useLayoutEffect(() => {
    setRows(!!defaultSortBy ? stableSort(items, order, orderBy) : items)
  }, [order, orderBy, loading])

  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === ORDER.ASC
    setOrder(isAsc ? ORDER.DESC : ORDER.ASC)
    setOrderBy(property)
  }

  return (
    <Paper elevation={2}>
      <MuiTableContainer className="tableComponent">
        <MuiTable aria-labelledby="mainTable" size="small" aria-label="main table" className={(loading && classes.tableLoading) || ''}>
          <TableHeader
            headerCells={headerCells}
            onRequestSort={handleRequestSort}
            order={order}
            orderBy={orderBy}
            totalItems={items.length}
          />
          {!loading && (
            <tbody>
              {rows.map((row) => {
                const rowId = (columnId && row[columnId]) || row.id
                const {cpu,memory,name,uptime} = row
                return (
                  <tr className={classes.tr} key={rowId}>
                    {headerCells.map((headerCell) => (
                      <td key={autoGenId([headerCell.id, rowId, uuidv4()], 'table-cell')} id={autoGenId([headerCell.id, rowId], 'table-cell')} className={classes.td}>
                        {headerCell.component
                          ? headerCell.component({ cpu, memory, name,uptime})
                          : row[headerCell.id]}
                      </td>
                    ))}
                  </tr>
                )
              })}
            </tbody>
          )}
        </MuiTable>
        {(loading || items.length === 0) && (
          <div className={classes.nodata}>
            {loading && <CircularProgress />}
            {!loading && items.length === 0 && <Typography variant="body1">No data to display</Typography>}
          </div>
        )}
      </MuiTableContainer>
    </Paper>
  )
}

Table.propTypes = {
  headerCells: arrayOf(
    shape({
      id: string,
      numeric: bool,
      label: string,
      sortable: bool,
      component: func,
    })
  ),
  items: arrayOf(object),
  defaultSort: oneOf([ORDER.ASC, ORDER.DESC]),
  defaultSortBy: string,
  id: string,
  loading: bool,
  columnId: string,
}

Table.defaultProps = {
  headerCells: [],
  items: [],
  defaultSort: ORDER.ASC,
  defaultSortBy: '',
  id: '',
  columnId: '',
  loading: false,
}

const checkChangeProps = (prevProps, nextProps) => {
  return (
    isEqual(prevProps.items, nextProps.items) &&
    prevProps.loading === nextProps.loading
  )
}

export default memo(Table, checkChangeProps)
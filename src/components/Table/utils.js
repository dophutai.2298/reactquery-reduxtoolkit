
import { ORDER } from './enums'

const formatSortValue = (value, func) => {
  const val = value && value.toString().toLowerCase()
  return func ? func(val) : val
}

const descendingComparator = (a, b, orderBy) => {
  const aLowerCase = formatSortValue(a[orderBy])
  const bLowerCase = formatSortValue(b[orderBy])
  if (bLowerCase < aLowerCase) {
    return -1
  }
  if (bLowerCase > aLowerCase) {
    return 1
  }
  return 0
}

const getComparator = (order, orderBy) => {
  return order === ORDER.DESC
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

export const stableSort = (items, order, orderBy) => {
  const stabilizedThis = items.map((item, index) => [item, index])
  const comparator = getComparator(order, orderBy)
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((item) => item[0])
}
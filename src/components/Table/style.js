import { makeStyles } from '@material-ui/core/styles'
import colors from '../../constants/colors'
const useStyles = makeStyles((theme) => ({
    tableLoading: {
        opacity: '0.5',
    },
    td: {
        padding: '4px 16px',
        color: colors.gray4,
        width: '15%',
    },
    tr: {
        '&:nth-of-type(odd) ': {
            backgroundColor: colors.gray2
        },
        '&input': {
            backgroundColor: colors.white
        },
        '&:hover ': {
            backgroundColor: colors.baby,
        },
        '&.selected': {
            backgroundColor: colors.baby,
            borderBottom: '0.5px $gray2 solid',
        }
    },
    nodata: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '60px',
        backgroundColor: colors.gray2,
        fontSize: '12px',
        fontWeight: 700,
        color: colors.gray4,
    }
}))

export default useStyles
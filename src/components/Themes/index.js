import colors from '../../constants/colors'
import overrides from './overrides'

export default {
  palette: {
    primary: {
      main: colors.cobalt,
      light: colors.cobalt,
      dark: colors.baby,
      contrastTexts: colors.white,
    },
    secondary: {
      main: colors.secondary,
      light: colors.red,
      dark: colors.red,
      contrastText: colors.white,
    },
    warning: {
      main: colors.burnt,
      light: colors.burnt,
      dark: colors.burnt,
    },
    success: {
      main: colors.success,
      light: colors.successDark,
      dark: colors.successLight,
    },
    info: {
      main: colors.info,
    },
    text: {
      primary: colors.black,
      secondary: colors.gray4,
      hint: '#B9B9B9',
      error: colors.red,
      white: colors.white,
      disabled: colors.gray4,
    },
    background: {
      blue: colors.cobalt,
      default: colors.white,
      light: colors.cobalt,
    },
    action: {
      hover: colors.baby,
      focus: colors.baby,
    },
  },
  customShadows: {
    widget: '0px 3px 11px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A',
    widgetDark: '0px 3px 18px 0px #4558A3B3, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A',
    widgetWide: '0px 12px 33px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A',
  },
  shape: {
    borderRadius: 4,
  },
  ...overrides,
  background: colors.sea,
}
import colors from '../../constants/colors'

export default {
  overrides: {
    MuiAppBar: {
      root: {
        backgroundColor: colors.federal,
      },
      colorPrimary: {
        backgroundColor: colors.federal,
      },
    },
    MuiToolbar: {
      root: {
        flexGrow: 1,
        flexWrap: 'wrap',
        padding: '0 15',
        justifyContent: 'space-evenly',
        display: 'flex',
        minHeight: 50,
      },
      regular: {
        minHeight: 50,
      },
    },
    // MuiTableSortLabel: {
    //   root: {
    //     color: colors.gray4,
    //   },
    //   active: {
    //     color: colors.gray4,
    //   },
    // },
    // MuiCheckbox: {
    //   root: {
    //     color: colors.gray4,
    //     '&$checked': {
    //       color: colors.cobalt,
    //     },
    //     '&:focus': {
    //       outline: `${colors.baby} auto 5px`,
    //       color: colors.baby,
    //     },
    //     '&:hover': {
    //       backgroundColor: 'transparent',
    //     },
    //   },
    //   colorSecondary: {
    //     color: colors.gray4,
    //     '&$checked': {
    //       color: colors.cobalt,
    //     },
    //     '> span > input &:focus': {
    //       outline: `${colors.baby} auto 5px`,
    //       color: colors.baby,
    //     },
    //   },
    // },
    MuiIconButton: {
      root: {
        color: colors.maya,
        padding: 4,
      },
      colorInherit: {
        color: colors.maya,
        '&:hover': {
          color: colors.white,
        },
      },
    },
    MuiButton: {
      root: {
        padding: 'inherit',
      },
    },
    MuiInputBase: {
      root: {
        padding: '0 6px',
        height: 28,
        margin: 2,
        borderRadius: 4,
        border: 'none',
        backgroundColor: colors.gray2,
        borderColor: colors.gray2,
        borderWidth: 1,
        borderStyle: 'solid',
        '&:hover, &.Mui-focused': {
          borderRadius: 4,
          borderColor: colors.capri,
          borderWidth: 1,
          borderStyle: 'solid',
          boxShadow: `1px 1px 5px 1px ${colors.baby}, -1px -1px 5px 1px ${colors.baby}`,
        },
      },
      input: {
        border: 'none',
        width: '100%',
        padding: 6,
      },
    },
    MuiOutlinedInput: {
      input: {
        padding: '8px 0',
        border: 'none',
      },
    },
    MuiInput: {
      underline: {
        '&:hover:not(.Mui-disabled):before': {
          border: 'none',
          borderBottomStyle: 'none',
        },
        '&:hover.Mui-disabled': {
          borderColor: colors.gray2,
          boxShadow: 'none',
        },
        '&.Mui-disabled:before': {
          borderColor: colors.gray2,
          borderBottomStyle: 'none',
        },
        '&:before': {
          borderBottomStyle: 'none',
        },
        '&:after': {
          borderBottomStyle: 'none',
        },
      },
    },
    MuiSelect: {
      select: {
        '&:focus': {
          backgroundColor: 'inherit',
        },
      },
    },
    // MuiAccordion: {
    //   root: {
    //     marginTop: 6,
    //     borderColor: colors.gray3,
    //     borderStyle: 'solid',
    //     borderWidth: 1,
    //     boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
    //     '&:before': {
    //       display: 'none',
    //     },
    //     '&$expanded': {
    //       margin: '6px 0',
    //     },
    //     '&.MuiPaper-root.Mui-expanded:first-child': {
    //       marginTop: '8px',
    //     },
    //   },
    //   rounded: {
    //     '&:first-child': {
    //       borderTopLeftRadius: 8,
    //       borderTopRightRadius: 8,
    //     },
    //     '&:last-child': {
    //       borderBottomLeftRadius: 8,
    //       borderBottomRightRadius: 8,
    //     },
    //   },
    // },
    // MuiAccordionSummary: {
    //   root: {
    //     '&.Mui-expanded': {
    //       minHeight: 4,
    //     },
    //   },
    //   content: {
    //     color: colors.gray4,
    //     fontSize: 16,
    //     alignItems: 'center',
    //     '&$expanded svg': {
    //       transform: 'rotate(90deg)',
    //     },
    //     '&.Mui-expanded': {
    //       margin: '8px 0',
    //     },
    //   },
    // },
    // MuiAccordionDetails: {
    //   root: {
    //     display: 'block ',
    //     height: '100%',
    //     padding: '0 16px 16px',
    //   },
    // },
    //   MuiListItem: {
    //     root: {
    //       width: 'inherit',
    //       paddingTop: 0,
    //       paddingBottom: 0,
    //       '&.Mui-selected:': {
    //         backgroundColor: colors.baby,
    //       },
    //     },
    //   },
    MuiLinearProgress: {
      root: {
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
        borderRadius: 4,
        height: 20
      },
    },
    MuiTab: {
      wrapper: {
        lineHeight: 1,
      },
    },
    //   MuiDialogTitle: {
    //     root: {
    //       padding: '0 12px 0 24px',
    //     },
    //   },
    //   MuiDialogContent: {
    //     dividers: {
    //       paddingTop: 0,
    //     },
    //   },
    MuiSwitch: {
      root: {
        width: 42,
        height: 26,
        padding: 0,
        margin: '8px'
      },
      switchBase: {
        padding: 1,
        '&$checked': {
          transform: 'translateX(16px)',
          '& + $track': {
            opacity: 1,
            border: 'none',
          },
        },
      },
      colorSecondary: {
        '&$checked': {
          transform: 'translateX(16px)',
          '& + $track': {
            backgroundColor: '#388e3c',
            opacity: 1,
            border: 'none',
          },
        },
      },
      thumb: {
        width: 24,
        height: 24,
        color: '#ffffff',
      },
      track: {
        borderRadius: 26 / 2,
        border: 'none',
        backgroundColor: '#bdbdbd',
        opacity: 1,
      },
    },
    //   MuiFormControl: {
    //     marginNormal: {
    //       marginTop: 0,
    //       marginBottom: 0,
    //     },
    //   },
  },
  typography: {
    fontFamily: 'Open Sans, sans-serif',
    h1: {
      fontSize: 24,
      fontWeight: 500,
    },
    h2: {
      fontSize: 24,
      fontWeight: 400,
    },
    h3: {
      fontSize: 18,
      fontWeight: 400,
    },
    h4: {
      fontSize: 16,
      fontWeight: 600,
    },
    h5: {
      fontSize: 16,
      fontWeight: 400,
    },
    h6: {
      fontSize: 14,
      fontWeight: 600,
    },
    p: {
      fontSize: 12,
    },
    body1: {
      fontSize: 12,
      fontWeight: 'regular',
    },
    body2: {
      fontSize: 10,
      fontWeight: 'regular',
    },
    button: {
      fontSize: 12,
      fontWeight: 'regular',
    },
    footer: {
      fontWeight: 400,
      fontSize: 10,
    },
    subtitle2: {
      fontWeight: 700,
      fontSize: 12,
    },
  },
}
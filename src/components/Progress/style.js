import { makeStyles } from '@material-ui/core/styles'
import colors from '../../constants/colors'
const useStyles = makeStyles((theme) => ({
    barNormal: {
        backgroundColor: colors.success
    },
    colorNormal: {
        backgroundColor: '#bcd8af'
    },
    barWarning: {
        backgroundColor: colors.warning
    },
    colorWarning: {
        backgroundColor: '#f4cda5'
    },
    barError: {
        backgroundColor: colors.red
    },
    colorError: {
        backgroundColor: '#e88c8c'
    }

}))

export default useStyles
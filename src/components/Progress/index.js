import React, { memo } from 'react'
import MuiLinearProgress from '@material-ui/core/LinearProgress'
import { autoGenId } from '../../constants/common'
import { number, oneOf, string } from 'prop-types'
import clsx from 'clsx'
import useStyles from './style'
import { Box, Typography } from '@material-ui/core'

const LinearProgress = (props) => {
  const { value, variant, className } = props
  const classes = useStyles()
  const bar = clsx(value <= 50 && classes.barNormal || value <= 80 && classes.barWarning || classes.barError)
  const colorPrimary = clsx(value <= 50 && classes.colorNormal || value <= 80 && classes.colorWarning || classes.colorError)

  return (
    <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
        <MuiLinearProgress
          id={autoGenId('', 'linear-progress')}
          className={className}
          classes={{ colorPrimary: colorPrimary, bar: bar }}
          variant={variant}
          value={value} />
      </Box>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">
          {`${Math.round(
            props.value*100,
          )/100}%`}
        </Typography>
      </Box>
    </Box>
  )
}

LinearProgress.propTypes = {
  value: number,
  variant: oneOf(['buffer', 'determinate', 'indeterminate', 'query']),
  className: string,
}

LinearProgress.defaultProps = {
  value: null,
  variant: 'indeterminate',
  className: null,
}

export default memo(LinearProgress)
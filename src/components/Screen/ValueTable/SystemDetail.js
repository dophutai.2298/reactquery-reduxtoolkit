import React from 'react'
import dayjs from 'dayjs'
import { Grid } from '@material-ui/core'
import ContentTab from '../../ContentTab'
export const SystemDetail=({ metric_data, button, grid, width, height }) =>{
    const systemLoadFormat = []
    const commandserverFormat = []
    const cpuFormat = []
    const memoryFormat = []
    const swapMemoryFormat = []


    metric_data?.virtual_memory.forEach(function (val) {
        let { timestamp, total, used } = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        memoryFormat.push({
            timestamp: formatTime,
            total: total,
            used: used,
        })
    })

    metric_data?.commandserver_stats.forEach(function (val) {
        let { timestamp, cs_cpu_usage, cs_mem_usage } = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        commandserverFormat.push({
            timestamp: formatTime,
            cs_cpu_usage: cs_cpu_usage,
            cs_mem_usage: cs_mem_usage,
        })
    })

    metric_data?.cpu_times.forEach(function (val) {
        let { timestamp, usage_iowait, usage_system, usage_user } = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        cpuFormat.push({
            timestamp: formatTime,
            usage_iowait: usage_iowait,
            usage_system: usage_system,
            usage_user: usage_user
        })
    })

    metric_data?.system_load.forEach(function (val) {
        let { load1, load5, load15, n_cpus, timestamp } = val
        let timeFormat = dayjs(timestamp).format("MMM-DD HH:mm")
        systemLoadFormat.push({
            timestamp: timeFormat,
            load1: Math.round(load1 * 1000) / 1000,
            load5: Math.round(load5 * 1000) / 1000,
            load15: Math.round(load15 * 1000) / 1000,
            n_cpus: n_cpus
        })
    })



    metric_data?.swap_memory.forEach(function (val) {
        let { timestamp, total, used } = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        swapMemoryFormat.push({
            timestamp: formatTime,
            total: total,
            used: used,
        })
    })

    return (
        <>
            {button}
            <h1>{width}</h1>
            <Grid container>
                {grid > 4 && <Grid item xs={1}></Grid>}
                <Grid item xs={grid}>
                    <p>System Load</p>
                    <ContentTab
                        width={width}
                        height={height}
                        data={systemLoadFormat}
                        label='timestamp'
                        items={[
                            { name: 'Load 1', dataKey: 'load1', color: '#28a4e6' },
                            { name: 'Load 15', dataKey: 'load15', color: '#ccaf01' },
                            { name: 'Load 15', dataKey: 'load5', color: '#f21' }
                        ]}
                    />
                </Grid>
                {grid > 4 && <Grid item xs={1}></Grid>}

                {grid > 4 && <Grid item xs={1}></Grid>}
                <Grid item xs={grid}>
                    <p>CommandServer</p>
                    <ContentTab
                        width={width}
                        height={height}
                        data={commandserverFormat}
                        label='timestamp'
                        items={[
                            { name: 'Cpu Utilization', dataKey: 'cs_cpu_usage', color: '#28a4e6' },
                            { name: 'Memory Utilization', dataKey: 'cs_mem_usage', color: '#ccaf01' },

                        ]}
                    />
                </Grid>
                {grid > 4 && <Grid item xs={1}></Grid>}

                {grid > 4 && <Grid item xs={1}></Grid>}
                <Grid item xs={grid}>
                    <p>CPU Times</p>
                    <ContentTab
                        width={width}
                        height={height}
                        data={cpuFormat}
                        label='timestamp'
                        items={[
                            { name: 'IOWait', dataKey: 'usage_iowait', color: '#28a4e6' },
                            { name: 'System', dataKey: 'usage_system', color: '#ccaf01' },
                            { name: 'User', dataKey: 'usage_user', color: '#f21' }
                        ]}
                    />
                </Grid>
                {grid > 4 && <Grid item xs={1}></Grid>}

                {grid > 4 && <Grid item xs={1}></Grid>}
                <Grid item xs={grid}>
                    <p>Memory Utilization</p>
                    <ContentTab
                        width={width}
                        height={height}
                        data={memoryFormat}
                        label='timestamp'
                        items={[
                            { name: 'Total', dataKey: 'total', color: '#28a4e6' },
                            { name: 'Used', dataKey: 'used', color: '#ccaf01' },
                        ]}
                    />
                </Grid>
                {grid > 4 && <Grid item xs={1}></Grid>}

                {grid > 4 && <Grid item xs={1}></Grid>}
                <Grid item xs={grid}>
                    <p>Swap Memory</p>
                    <ContentTab
                        width={width}
                        height={height}
                        data={swapMemoryFormat}
                        label='timestamp'
                        items={[
                            { name: 'Total', dataKey: 'total', color: '#28a4e6' },
                            { name: 'Used', dataKey: 'used', color: '#ccaf01' },
                        ]}
                    />
                </Grid>
                {grid > 4 && <Grid item xs={1}></Grid>}

            </Grid>
        </>

    )
}

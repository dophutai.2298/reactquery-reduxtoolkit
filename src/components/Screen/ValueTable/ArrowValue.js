import React from 'react'
import { useQuery } from 'react-query';
import { fetchMetric } from '../../../api/actions';
import Arrow from '../../Child/Arrow';
import {compareTime} from './ComparisonDay'

export const ArrowValue = ({ name, cpu, uptime,memory }) => {
    const { isLoading, data: data_metric} = useQuery(
        ['metric_data', name],
        () => fetchMetric(name))

    let prevCpu = 0
    data_metric?.cpu_times.forEach(function (val,i) {
        let { timestamp, usage_iowait, usage_system, usage_user } = val
        if (compareTime(timestamp,uptime)) {
            return prevCpu = usage_iowait + usage_system + usage_user
        }
    })

    let prevMem = 0
    data_metric?.virtual_memory?.forEach(function (val, i) {
        let { used, total,timestamp } = val
        if (compareTime(timestamp,uptime)) {
            let percent = used / total * 100
            return prevMem = Math.round(percent * 100) / 100
        }
    })

    return (
        <div>
            {isLoading && <>..</>}
            {data_metric && cpu &&  <Arrow prev={prevCpu} now={cpu} />}
            {data_metric && memory &&  <Arrow prev={prevMem} now={memory} />}
           
        </div>
    )
}

import React from 'react'
import dayjs from 'dayjs'
import { Grid } from '@material-ui/core'
import ContentTab from '../../ContentTab'

export const StorageDetail=({ metric_data, button, grid, width, height })=> {
    const diskLatencyFormat=[]
    const diskRWOperationFormat=[]
    const partitionFormat=[]

    metric_data?.disk_latency.forEach(function (val) {
        let { timestamp,io_time} = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        diskLatencyFormat.push({
            timestamp: formatTime,
            io_time: io_time,
        })
    })
    
    metric_data?.disk_partitions.forEach(function (val) {
        let { timestamp,boot,home,replicated,root } = val
        let variable=val.var
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        partitionFormat.push({
            timestamp: formatTime,
            boot: boot,
            home:home,
            replicated:replicated,
            root:root,
            var:variable
        })
    })

    metric_data?.disk_ew_ops.forEach(function (val) {
        let { timestamp,read_bytes,write_bytes} = val
        let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
        diskRWOperationFormat.push({
            timestamp: formatTime,
            read_bytes: read_bytes,
            write_bytes:write_bytes
        })
    })

    return (
        <>
        {button}
        <h1>{width}</h1>
        <Grid container>
            {grid > 4 && <Grid item xs={1}></Grid>}
            <Grid item xs={grid}>
                <p>Disk Latency</p>
                <ContentTab
                    width={width}
                    height={height}
                    data={diskLatencyFormat}
                    label='timestamp'
                    items={[
                        { name: 'Disk Latency', dataKey: 'io_time', color: '#28a4e6' }
                    ]}
                />
            </Grid>
            {grid > 4 && <Grid item xs={1}></Grid>}

            {grid > 4 && <Grid item xs={1}></Grid>}
            <Grid item xs={grid}>
                <p>Disk R/W Operations</p>
                <ContentTab
                    width={width}
                    height={height}
                    data={diskRWOperationFormat}
                    label='timestamp'
                    items={[
                        { name: 'Read', dataKey: 'read_bytes', color: '#28a4e6' },
                        { name: 'Write', dataKey: 'write_bytes', color: '#ccaf01' }
                    ]}
                />
            </Grid>
            {grid > 4 && <Grid item xs={1}></Grid>}

            {grid > 4 && <Grid item xs={1}></Grid>}
            <Grid item xs={grid}>
                <p>Partitions Usage</p>
                <ContentTab
                    width={width}
                    height={height}
                    data={partitionFormat}
                    label='timestamp'
                    items={[
                        { name: '/ROOT', dataKey: 'root', color: '#28a4e6' },
                        { name: '/boot', dataKey: 'boot', color: '#ccaf01' },
                        { name: '/home', dataKey: 'home', color: '#ff0000' },
                        { name: '/var', dataKey: 'var', color: '#166641' },
                        { name: '/replicated', dataKey: 'replicated', color: '#38627c' },
                    ]}
                />
            </Grid>
            {grid > 4 && <Grid item xs={1}></Grid>}
        </Grid>
        </>
    )
}

import React from 'react'
import { useQuery } from 'react-query';
import { fetchMetric } from '../../../api/actions';
import { LineChartCustomize } from '../LineCustomize';
import {compareTime} from './ComparisonDay'

export const  LineChartValue=({name,disk})=> {
    const { isLoading, data: data_metric} = useQuery(
        ['metric_data', name],
        () => fetchMetric(name))
    return (
        <>
        {isLoading && <p>...</p>}
        {data_metric &&  <LineChartCustomize
                    width={200}
                    height={30}
                    data={disk ? data_metric?.disk_ew_ops : data_metric?.eth0_tracffic_bytes}
                    items={disk ?( [
                        { dataKey: 'read_bytes', color: '#28a4e6' },
                        { dataKey: 'write_bytes', color: '#a25' }
                    ]):
                (
                    [
                        { dataKey: 'recv', color: '#28a4e6' },
                        { dataKey: 'sent', color: '#a25' }
                    ] 
                )}
                />}
           
        </>
    )
}

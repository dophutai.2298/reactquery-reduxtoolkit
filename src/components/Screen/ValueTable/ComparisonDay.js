import React from 'react'

export const compareTime=(previous, now)=> {
        //previous + 24h = now
        const nowDay = new Date(now).getTime()
        const previousDay = new Date(previous).getTime() + 86400000
        return nowDay === previousDay

}

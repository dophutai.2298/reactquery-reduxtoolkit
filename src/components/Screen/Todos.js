import React,{useState,useEffect} from 'react'
import { useQuery, useQueryClient,useMutation,QueryCache } from 'react-query'
import { decrement, increment } from '../../config/todoSlice'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProduct, removeProduct } from '../../api/actions'
import { NavLink } from 'react-router-dom'


const Todos = () => {
    const queryClient = useQueryClient()
    const { data, status,refetch } = useQuery('todo', fetchProduct,{
        onSuccess:()=>{
            queryClient.getQueryCache().subscribe()
        }
    })
    // const queryCache = new QueryCache({
    //     onError: error => {
    //       console.log(error)
    //     },
    //   })
    //   const query = queryCache.findAll('todo')
    //   console.log(query)
    //  const queryCache=new QueryCache()
    //  console.log('queryCache',queryCache)
    
    useEffect(() => {
        if (data?.hasMore) {
            queryClient.prefetchQuery('todo', () =>
            fetchProduct()
            )
        }
    }, [data])
    // data => removeProduct(data,{
    //     onSuccess:()=>{
    //         console.log('onSuccess')
    //         queryClient.invalidateQueries('todo')
    //         refetch()
    //     }
    // })

    const { mutate, isLoading } = useMutation(removeProduct,{
        onSuccess:()=>{
            // QueryCache.refetchQueries('todo')
            queryClient.refetchQueries('todo')
            // queryClient.setQueriesData('todo',(current)=>[
            //     ...current
            //     ])
        }
    })
   
    const dispatch = useDispatch();
    const number = useSelector(state => state.todos)

    // Dispatch to redux toolkit
    const handleAdd = () => {
        dispatch(increment(1))
    }

    const handleRemove = () => {
        dispatch(decrement(1))
    }
    //End Dispatch to redux toolkit

    const handleDelete = (id) => {
        mutate(id)
    }
    const [Id, setId] = useState(null)
    return (
        <div>
            <h2>{number}</h2>
            <button onClick={handleRemove}>-</button>
            <button onClick={handleAdd}>+</button>
            <div><hr />
                <h2>Product</h2>
                {status === 'error' && (
                    <div>Error fetching data</div>
                )}
                {status === 'loading' && (
                    <div>Loading..........</div>
                )}
                { isLoading &&( <div>..loading</div>)}
                {status === 'success' && (
                    <div>
                        {data?.map(item =>( <div>{item.name} - <button onClick={() => handleDelete(item.id)}>Remove</button><button onClick={()=>setId(item.id)}><NavLink exact to={`/detail/${item.id}`}>View</NavLink></button></div>))}
                    </div>
                )}
            </div>
        </div>
    )
}

export default Todos
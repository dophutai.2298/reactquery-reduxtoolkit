import React from 'react'

export default function Planet({planet}) {
    return (
        <div>
            <h3>{planet.name}</h3>
            <p>population-{planet.population}</p>
            <p>terrain-{planet.terrain}</p>
            <button>Remove</button>
        </div>
    )
}

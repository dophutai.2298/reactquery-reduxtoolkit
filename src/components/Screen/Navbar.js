import React from 'react'
import { NavLink } from 'react-router-dom'
export default function Navbar({ setPage }) {
  return (
    <nav>
      <ul>
        <li> <NavLink exact to="/planets">Planets</NavLink></li>
        <li>  <NavLink exact to="/people">People</NavLink></li>
        <li><NavLink exact to="/todos">Todos</NavLink></li>
        <li><NavLink exact to="/chart">Chart</NavLink></li>
        <li><NavLink exact to="/table">Table</NavLink></li>
      </ul>
    </nav>
  )
}

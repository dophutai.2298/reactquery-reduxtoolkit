import React from 'react'
import { useQuery } from 'react-query'
import { fetchDetail } from '../../api/actions'
import { useLocation } from "react-router-dom";

export default function Detail({Id}) {
    const location = useLocation();
    const id = location.pathname.substr(8);
    const {data,isLoading}=useQuery(['detail',id],()=>fetchDetail(id))
    if(isLoading) return <div>...Loading</div>
    return (
        <div>
            <h2>{data.name}</h2>
            <h2>{data.price}</h2>
        </div>
    )
}

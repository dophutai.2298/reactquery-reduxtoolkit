import React from 'react'
import { bool, number, arrayOf, object,node,shape } from 'prop-types'

import {
    LineChart,
    Line
} from 'recharts';
export const LineChartCustomize = ({ width, height, data, items, dot }) => {
    return (
        <LineChart width={width} height={height} data={data}>
            {/* strokeWidth: thickness, dot=bool: show or hide dot */}
            {items?.map((item,index)=><Line key={item.dataKey,'-',index} type='monotone' dot={dot} dataKey={item.dataKey} stroke={item.color} strokeWidth={1} />)} 
        </LineChart>
    )
}

LineChartCustomize.propTypes = {
    width: number,
    height: number,
    data: arrayOf(object),
    items: arrayOf(
        shape({
            color: node.isRequired,
            dataKey: node.isRequired
        }),
    ).isRequired,
    dot: bool
}

LineChartCustomize.defaultProps = {
    width: 200,
    height: 30,
    dot: false
}

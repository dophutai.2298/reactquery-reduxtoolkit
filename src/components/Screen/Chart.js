import React from 'react'
import {
    LineChart,
    Line,
    ResponsiveContainer,
    ComposedChart,
    Area,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from 'recharts';
import dayjs from 'dayjs'
import Arrow from '../Child/Arrow';
export default function Chart() {
    const uptime= "Wed, 02 Jun 2021 07:40:00 GMT"
    const prev="Tue, 01 Jun 2021 07:40:00 GMT"

    let now = dayjs(uptime);
    let prevtime=dayjs(prev)
    let df1 = now.diff(prevtime,"minute"); 
    // console.log('df1',df1);

    // console.log(nowFormat)
    // console.log(prevFormat)
    // console.log(now.format("MMM-DD-YYYY"));
    // console.log(now.format("HH:mm"))
    // let d1 = dayjs("2018-06-03");
    // console.log(d1.format());
    // let d2 = dayjs.unix(1530471537);
    // console.log(d2.format());


    const data = [
        {
            timestamp: 'Page A', uv: 590, pv: 800, amt: 1400,
        },
        {
            timestamp: 'Page B', uv: 868, pv: 967, amt: 1506,
        },
        {
            timestamp: 'Page C', uv: 1397, pv: 1098, amt: 989,
        },
        {
            timestamp: 'Page D', uv: 1480, pv: 1200, amt: 1228,
        },
        {
            timestamp: 'Page E', uv: 1520, pv: 1108, amt: 1100,
        },
        {
            timestamp: 'Page F', uv: 1400, pv: 680, amt: 1700,
        },
    ];

    const [opacity, setOpacity] = React.useState({ uv: 1, pv: 1, amt: 1 })


    const handleMouseEnter = (o) => {
        const { dataKey } = o
        setOpacity({
            ...opacity, [dataKey]: 0.5
        })
    }

    const handleMouseLeave = (o) => {
        const { dataKey } = o
        setOpacity({
            ...opacity, [dataKey]: 1
        })
    }

    return (
        <div>
            {/* <Arrow prev='1' now='1' />
            <Arrow prev='1' now='1.05' />
            <Arrow prev='1' now='1.7' />
            <Arrow prev='1' now='2.1' />
            <Arrow prev='1.05' now='1' />
            <Arrow prev='1.4' now='1' />
            <Arrow prev='1.7' now='1' /> */}
            <h2>Chart table</h2>
            <LineChart width={300} height={100} data={data}>
                {/* strokeWidth: Thickness, dot=bool: show or hidden dot */}
                <Line type='monotone' dot={false} dataKey='amt' stroke='#f12' strokeWidth={1} />
                {/* <Line type='monotone' dot={false} dataKey='amt' stroke='#222' strokeWidth={1} /> */}
            </LineChart>
            <LineChart width={300} height={100} data={data}>
                {/* strokeWidth: Thickness, dot=bool: show or hidden dot */}
                <Line type='monotone' dot={false} dataKey='pv' stroke='#212' strokeWidth={1} />
                {/* <Line type='monotone' dot={false} dataKey='amt' stroke='#222' strokeWidth={1} /> */}
            </LineChart>
            <h2>Chart component</h2>
            <div style={{ width: 500, height: 300 }}>
                <ResponsiveContainer>
                    <ComposedChart
                        width={500}
                        height={400}
                        data={data}
                        margin={{
                            top: 20, right: 20, bottom: 20, left: 20,
                        }}
                    >
                        {/* Grid chart */}
                        <CartesianGrid stroke="#f5f5f5" />

                        {/* Label col X */}
                        <XAxis dataKey="timestamp" style={{ fontSize: '12px' }} angle={-45} textAnchor="end" allowDuplicatedCategory={true} />

                        {/* Col Y */}
                        <YAxis />

                        {/* Show more info when hover in row */}
                        <Tooltip />

                        {/* Note line for chart */}
                        <Legend wrapperStyle={{ fontFamily: 'Roboto, sans-serif', fontSize: '12px', padding: '20px' }}
                            verticalAlign="bottom"
                            height={5}
                            onMouseEnter={handleMouseEnter}
                            onMouseLeave={handleMouseLeave}
                        />
                        {/* connectNulls: When data null, The chart will connect between 2 points                   */}
                        <Line connectNulls name="Apples" strokeOpacity={opacity.pv} type="monotone" dataKey="pv" stroke="#ff7300" />
                        <Line connectNulls  name="Bananas" strokeOpacity={opacity.amt} type="monotone" dataKey="amt" stroke="#000" />
                        <Line connectNulls name="Grapes" strokeOpacity={opacity.uv} type="monotone" dataKey="uv" stroke="#ccc" />
                    </ComposedChart>
                </ResponsiveContainer>
            </div>
        </div>
    )
}

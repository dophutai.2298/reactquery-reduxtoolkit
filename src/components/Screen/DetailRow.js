import React from 'react'
import { QueryCache, useQueryClient } from 'react-query'
import MainTabs from '../MainTabs'
import SettingsIcon from '@material-ui/icons/Settings';
import { Button } from '../Button';
import { Grid } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import { useLocation } from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { NavLink } from 'react-router-dom';
import { SystemDetail } from './ValueTable/SystemDetail';
import { StorageDetail } from './ValueTable/StorageDetail';
import { NetworkDetail } from './ValueTable/NetworkDetail';

const DetailRow = () => {

    const location = useLocation();
    const name = location.pathname.substr(7);
    const queryClient = useQueryClient()
    const metric_data = queryClient.getQueryData(["metric_data", name])
    const widthScreen = window.screen.width
    const heightScreen = window.screen.height

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const [row, setRow] = React.useState({ grid: 4, width: widthScreen * 0.25, height: heightScreen * 0.3 })
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleARow = () => {
        setRow({
            grid: 10, width: widthScreen * 0.5, height: heightScreen * 0.35
        })
        setAnchorEl(null);
    }

    const handle3Rows = () => {
        setRow({ grid: 4, width: widthScreen * 0.25, height: heightScreen * 0.3 })
        setAnchorEl(null);
    }

    const ButtonCustomize = () => {
        return (
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button startIcon={<SettingsIcon />} label="Customize" size="md" onClick={handleClick} aria-controls="simple-menu" aria-haspopup="true" />
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    style={{ marginTop: '30px' }}
                >
                    <Paper style={{ boxShadow: 'none' }}>
                        <MenuItem onClick={handleARow}>1 row</MenuItem>
                        <MenuItem onClick={handle3Rows}>3 rows</MenuItem>
                    </Paper>
                </Menu>
            </div>
        )

    }



    const tabItems = [
        { label: 'System', value: 'system', content: <div style={{ marginTop: '10px' }}><SystemDetail metric_data={metric_data} grid={row.grid} width={row.width} height={row.height} /></div> },
        { label: 'Network', value: 'network', content: <div style={{ marginTop: '10px' }}><NetworkDetail metric_data={metric_data} grid={row.grid} width={row.width} height={row.height} /></div> },
        { label: 'Storage', value: 'storage', content: <div style={{ marginTop: '10px' }}><StorageDetail metric_data={metric_data} grid={row.grid} width={row.width} height={row.height} /></div> },
        { label: 'DNS', value: 'dns', content: <div style={{ marginTop: '10px' }}>DNS</div> },
    ]
    return (
        <div>
            <h2>{name}</h2>
            <NavLink to='/table' style={{ textDecoration: 'none', color: '#222' }}>
                <ArrowBackIcon />
            </NavLink>
            {ButtonCustomize()}
            <MainTabs tabItems={tabItems} defaultTab="system" />





        </div>
    )
}
export default DetailRow
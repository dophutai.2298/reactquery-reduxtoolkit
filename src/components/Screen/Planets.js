import React,{useState,useEffect} from 'react'
import { useQuery,useMutation } from 'react-query'
import Planet from './Planet'

const fetchPlanets = async () => {
    const res = await fetch(`https://swapi.dev/api/planets/?page=1`)
    return res.json()
}
const Planets = () => {
    const [page, setPage] = useState(1)
    const { data, status } = useQuery('planets',fetchPlanets)

    // ,{
    //      staleTime:2000
    //     onSuccess:()=>console.log('Data fetch successfully')
    // })
    return (
        <div>
            <h2>Planets</h2>
            {status==='error'&&(
                <div>Error fetching data</div>
            )}
             {status==='loading'&&(
                <div>Loading..........</div>
            )}
              {status==='success'&&(
                <div>
                    {data?.results?.map(planet=><div><Planet key={planet.name} planet={planet}/></div>)}
                </div>
            )}
        </div>


    )
}
export default Planets
import React, { useEffect, useState } from 'react'
import Table from '../Table'
import LinearProgress from '../../components/Progress';
import { LineChart, Line } from 'recharts'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { ORDER } from '../../components/Table/enums'
import { useQuery, useQueryClient } from 'react-query'
import { fetchMetric, fetchServer } from '../../api/actions';
import Arrow from '../Child/Arrow';
import dayjs from 'dayjs'
import { LineChartCustomize } from './LineCustomize';
import { Button } from '@material-ui/core';
import { ArrowValue } from './ValueTable/ArrowValue';
import { LineChartValue } from './ValueTable/LineChartValue';
import { NavLink } from 'react-router-dom';
export default function TableScreen() {

    const queryClient = useQueryClient()

    const { data, isLoading, isError, isSuccess } = useQuery('server', fetchServer)

    // -- Xử lý ra 1 mảng mới của Ram đã sd trước đó. used/total
    // dataMetric?.virtual_memory?.forEach(function (val, index) {
    //     let { timestamp } = val
    //     let prevTime = dayjs(timestamp)
    //     let { used, total } = val
    //     let percent = used / total * 100
    //     let formatValue = Math.round(percent * 100) / 100
    //     ramFormat.push({
    //         timestamp: timestamp,
    //         memory: formatValue,
    //     })
    // })
    // ------------------------------------------

    //-- Xử lý ra 1 mảng mới của CPU đã sd trước đó. Tổng 3 thằng usage_iowait, usage_system, usage_user
    // dataMetric?.cpu_times.forEach(function (val, index) {
    //     let { timestamp, usage_iowait, usage_system, usage_user } = val
    //     let prevTime = dayjs(timestamp)
    //     let sum = usage_iowait + usage_system + usage_user
    //     cpuFormat.push(
    //         {
    //             timestamp: timestamp,
    //             cpu: sum,
    //         }

    //     )
    // })
    // ------------------------------------------

    // dataMetric?.disk_ew_ops.forEach(function (val, index) {
    //     let { timestamp, read_bytes, write_bytes } = val
    //     let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
    //     diskFormat.push({
    //         timestamp: formatTime,
    //         read_bytes: read_bytes,
    //         write_bytes: write_bytes
    //     })
    // })

    // dataMetric?.eth0_tracffic_bytes.forEach(function (val, index) {
    //     let { timestamp, sent, recv } = val
    //     let formatTime = dayjs(timestamp).format("MMM-DD HH:mm")
    //     networkFormat.push({
    //         timestamp: formatTime,
    //         sent: sent,
    //         recv: recv
    //     })
    // })




    //---- Xử lý thành 1 data server mới, với gia trị của MEmory (used/total)  =-> Server gồm 6 col
    let dataFormat = []
    data?.forEach((val, index) => {
        let { id, name, ipadress, cpu, system, uptime } = val
        let time = dayjs(uptime);
        // const disk = []
        // const network = []
        let { used, total } = val.memory
        let percent = used / total * 100
        let formatValue = Math.round(percent * 100) / 100
        dataFormat.push({
            id: id,
            name: name,
            ipadress: ipadress,
            cpu: cpu,
            memory: formatValue,
            system: system,
            // uptime: time.format("MMM-DD"),
            timeFormat: time.format("MMM-DD"),
            // cpuPrev: '',
            // memoryPrev: '',
            //  disk: [],
            //  network: [],
            uptime: uptime,
        })
    })


    const BorderLinearProgress = withStyles((theme) => ({
        root: {
            height: 10,
            borderRadius: 5,
        },
        colorPrimary: {
            backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
        },
        bar: {
            borderRadius: 5,
            backgroundColor: '#1a90ff',
        },
    }))(LinearProgress);


    const headerCells = [
        {
            id: 'name',
            label: 'Name',
            sortable: true,
            component: ({ name }) => (
                <NavLink to={`table/${name}`} style={{ textDecoration: 'none', color: '#222' }}>
                    {name}
                </NavLink>
            ),
        },
        {
            id: 'ipadress',
            label: 'IP Adress   ',
            sortable: true
        }
        ,
        {
            id: 'cpu',
            label: 'CPU',
            sortable: true,
            component: ({ cpu, uptime, name }) => (
                <div style={{
                    display: 'flex', width: '100%'
                }}>
                    <div style={{
                        width: '90%',
                    }}>
                        <BorderLinearProgress variant="determinate" value={cpu} />
                    </div>
                    <div style={{ width: '10%' }}>
                        {/* <TrendingArrow prev={cpuPrev} now={cpu} /> */}
                        <ArrowValue name={name} cpu={cpu} uptime={uptime} />
                    </div>

                </div>
            ),
        },
        {
            id: 'memory',
            label: 'Memory',
            sortable: true,
            component: ({ memory, uptime, name }) => (
                <div style={{
                    display: 'flex', width: '100%'
                }}>
                    <div style={{
                        width: '90%',
                    }}>
                        <BorderLinearProgress variant="determinate" value={memory} />
                    </div>
                    <div style={{ width: '10%' }}>
                        <ArrowValue name={name} memory={memory} uptime={uptime} />
                    </div>

                </div>
            ),
        },
        {
            id: 'network',
            label: 'Network Activity',
            sortable: true,
            component: ({ name }) => (
                <LineChartValue name={name} />

            )
        },
        {
            id: 'disk',
            label: 'Disk Activity',
            sortable: true,
            component: ({ name }) => (
                <>
                    <LineChartValue name={name} disk={true} />
                </>
            )
        },
        {
            id: 'system',
            label: 'System Version',
            sortable: true
        },
        {
            id: 'timeFormat',
            label: 'uptime',
            sortable: true
        }
    ]

    return (
        <div>
            <h2>Table</h2>
            {isLoading && (<div>....Loading</div>)}
            {isError && (<div>Error fetching data</div>)}

            {(isSuccess) && (
                <div>
                    <Table loading={isLoading} headerCells={headerCells} items={dataFormat} />
                    {/* defaultSort={ORDER.ASC} defaultSortBy={dataFormat[0].id} */}
                </div>
            )}
        </div>
    )
}

import React from 'react'
import { useQuery, useQueryClient } from 'react-query'
import Planet from './Planet'
import axios from 'axios'

async function fetchProjects(page = 1) {
    const { data } = await axios.get('https://swapi.dev/api/planets/?page=' + page)
    return data
}


const People = () => {
    const [page, setPage] = React.useState(1)
    const queryClient = useQueryClient()
    const { status, data, latestData, isPreviousData } = useQuery(
        ['projects', page],
        () => fetchProjects(page),
        { keepPreviousData: true, staleTime: 5000 }
    )
      
    // Prefetch the next page!
    React.useEffect(() => {
        if (data?.hasMore) {
            queryClient.prefetchQuery(['projects', page + 1], () =>
                fetchProjects(page + 1)
            )
        }
    }, [data, page, queryClient])
    console.log(latestData)
    console.log('isPreviousData',isPreviousData)
    console.log('hasMore',!data?.hasMore)
    return (
        <div>
            <h2>People</h2>
            <div>Current Page: {page}</div>
            <button
                        //onClick={() => setPage(old => Math.max(old - 1, 0))}
                       onClick={()=>setPage(page-1)}
                        disabled={page === 1}
                    >
                        Previous Page
                    </button>
                    <button
                        onClick={() => {
                             //setPage(old => (data?.hasMore ? old + 1 : old))
                           setPage(page+1)
                        }}
                          disabled={isPreviousData == !data?.hasMore}
                    >
                        Next Page
                    </button>
            {status === 'error' && (
                <div>Error fetching data</div>
            )}
            {status === 'loading' && (
                <div>Loading..........</div>
            )}
            {status === 'success' && (
                <div>
                    {data?.results?.map(planet => <div><Planet key={planet.name} planet={planet} /></div>)}
                </div>
            )}
        </div>
    )
}
export default People
import React, { memo } from 'react'
import Box from '@material-ui/core/Box'

const CustomTabPanel = ({ children }) => {
  return (
    <Box>
      {children}
    </Box>
  )
}

CustomTabPanel.defaultProps = {
  children: ''
}

export default memo(CustomTabPanel)
import React, { memo, useEffect, useState } from 'react'
import Box from '@material-ui/core/Box'
import { any, arrayOf, element, node, shape, string } from 'prop-types'
import isEqual from 'lodash/isEqual'
import CustomTab from './CustomTab'
import CustomTabs from './CustomTabs'
import CustomTabPanel from './CustomTabPanel'

const MainTabs = (props) => {
  const { tabItems, defaultTab,button } = props
  const [value, setValue] = useState('')

  useEffect(() => {
    setValue(defaultTab)
    if (!defaultTab && tabItems.length > 0) setValue(tabItems[0].value)
  }, [defaultTab, tabItems])

  const handleChange = (_, newValue) => {
    setValue(newValue)
  }

  return (
    tabItems.length > 0 &&
    value && (
      <>
        <Box style={{ boxShadow: '0 4px 4px -4px gray' }}>
          <CustomTabs
            value={value}
            variant="scrollable"
            indicatorColor="secondary"
            textColor="inherit"
            onChange={handleChange}
            aria-label="Main Tabs">
            {tabItems.map((item) => (
              <CustomTab
                key={`tab-label-${item.value}`}
                value={item.value}
                label={item.label}
                id={`tab-label-${item.value}`}
              />
            ))}
          </CustomTabs>
        </Box>
        <Box pt={1}>
          {tabItems.map((item) => {
            return (
              value.toString() === item.value && (
                <CustomTabPanel key={`tab-panel-${item.value}`}>
                 {item.content}
                </CustomTabPanel>
              )
            )
          })}
        </Box>
      </>
    )
  )
}

MainTabs.propTypes = {
  defaultTab: string,
  tabItems: arrayOf(
    shape({
      value: any,
      label: node,
      component: element,
    })
  ),
}

MainTabs.defaultProps = {
  defaultTab: '',
  tabItems: [],
}

const checkChangeTabItems = (prevProps, nextProps) => {
  return isEqual(prevProps.tabItems, nextProps.tabItems)
}

export default memo(MainTabs, checkChangeTabItems)
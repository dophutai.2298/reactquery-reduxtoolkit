import { makeStyles } from '@material-ui/core/styles'
import colors from '../../../constants/colors'
const useStyles = makeStyles((theme) => ({
    tabContainer: {
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: colors.gray3,
        borderTopRightRadius: '15px',
        borderTopLeftRadius: '3px',
        '&:hover': {
            backgroundColor: colors.baby,
        }
    },
    tabRoot: {
        textTransform: 'none',
        color: colors.black,
        fontWeight: 700,
        fontSize: '12px',
        padding: '0',
        height: '28px',
        minHeight: '28px',
        opacity: '0.4',
        '&:focus': {
            color: colors.black,
            opacity: 1,
        },
        '&:hover': {
            color: colors.black,
            opacity: 1,
        }
    },
    containerActive: {
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: colors.cobalt
    }
}))

export default useStyles
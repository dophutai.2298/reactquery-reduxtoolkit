import React, { memo } from 'react'
import clsx from 'clsx'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'
import { bool } from 'prop-types'
import useStyles from './style'

const CustomTab = (props) => {
  const { selected, ...subProps } = props
  const classes = useStyles()
  const containerActive = clsx(classes.tabContainer, selected && classes.containerActive)
  return (
    <Box className={containerActive}>
      <Tab disableRipple {...subProps} selected={selected} className={classes.tabRoot} />
    </Box>
  )
}

CustomTab.propTypes = {
  selected: bool
}

CustomTab.defaultProps = {
  selected: false,
}

export default memo(CustomTab)
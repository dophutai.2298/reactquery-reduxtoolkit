import React from 'react'
import Tabs from '@material-ui/core/Tabs'
import { withStyles } from '@material-ui/core/styles'

import colors from '../../../constants/colors'

const CustomTabs = withStyles({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    minHeight: 30,
  },
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    '& > span': {
      width: '100%',
      backgroundColor: colors.white,
    },
  },
})((props) => {
  return <Tabs {...props} />
})

export default CustomTabs
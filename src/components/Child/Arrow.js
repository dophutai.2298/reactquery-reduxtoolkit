import React, { memo } from 'react'
import { number } from 'prop-types'
// import useStyles from './style';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import DragHandleSharpIcon from '@material-ui/icons/DragHandleSharp';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    iconEqual:{
        color:'#aeaeae',
        fontSize:'16px',  
    },
    incSmall:{
        color:'#6ebc6e',
        fontSize:'12px'
    },
    incMedium:{
        color:'#6ebc6e',
        fontSize:'16px'
    },
    incBig:{
        color:'#6ebc6e',
        fontSize:'22px'
    },
    decSmall:{
        color:'#d62829',
        fontSize:'12px'
    },
    decMedium:{
        color:'#d62829',
        fontSize:'16px'
    },
    decBig:{
        color:'#d62829',
        fontSize:'22px',
    }
  }));
const Arrow = ({ prev, now }) => {
    const classes = useStyles()
    // console.log(prev,'-',now)
    const Comparation = () => {
        return prev / now * 100
    }

    const handleValue = (value) => {
        if (value < 100 && value >= 90) return classes.decSmall
        else if (value < 90 && value >= 50) return classes.decMedium
        else if (value < 50) return classes.decBig
        else if (value < 50) return classes.decBig
        else if (value > 100 && value <= 110) return classes.incSmall
        else if (value > 110 && value <= 150) return classes.incMedium
        else return classes.incBig
    }

    return (
        <div>
            {Comparation() == 100 ? <DragHandleSharpIcon className={classes.iconEqual} /> : Comparation() >= 100 ? (<ArrowUpwardIcon className={handleValue(Comparation())} />) : (<ArrowDownwardIcon  className={handleValue(Comparation())}  />)}
        </div>
    )
}

Arrow.protoType = {
    prev: number,
    now: number
}

Arrow.defaultProps={
    prev:1,
    now:1
}

export default Arrow
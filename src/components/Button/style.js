import { makeStyles } from '@material-ui/core/styles';
import colors from '../../constants/colors';

const useStyles = makeStyles(() => ({
    sm: {
        height: '24px',
        fontSize: '12px',
        padding:'5px'
    },
    md: {
        height: '30px',
        fontSize: '12px',
        padding:'5px'
    },
    label: {
        textTransform: 'capitalize',
        fontWeight: '600',
    },
    rootIcon: {
        transition: 'all .3s',
        '&:hover': {
            background: 'transparent',
        },
        '&:focus': {
            background: 'transparent',
        }
    },

    rootButton: {
        background: colors.primary,
        color: colors.white,
        border: `0.5px solid ${colors.primary}`,
        transition: 'all .3s',
        '&:hover': {
            background: colors.secondary,
            border: `0.5px solid ${colors.secondary}`,
        },
        '&:focus': {
            background: colors.primary,
            border: `0.5px solid ${colors.pictonblue}`,
        }
    },

    rootIconOutline: {
        transition: 'all .3s',
        width: '24px',
        height: '24px',
        border: '1px solid #fff',
        '&:hover': {
            color: colors.secondary,
            border: `1px solid ${colors.secondary}`
        },
        '&:focus': {
            color: colors.blue,
            border: `1px solid ${colors.blue}`
        }
    }
}))

export default useStyles
import React from 'react'
import { bool, element, func, node, oneOf, string } from 'prop-types'
import MuiButton from '@material-ui/core/Button'
import MuiTooltip from '@material-ui/core/Tooltip'
import MuiIconButton from '@material-ui/core/IconButton'
import useStyles from './style'
import clsx from 'clsx'

export const Button = (props) => {
    const { className, tooltip, size, icon, label, variant, iconBorder, onClick, startIcon, endIcon, ...subProps } = props
    const classes = useStyles()
    const button = icon ?
        (iconBorder ? (<MuiButton color="primary" variant={variant} className={clsx(size === 'sm' ? classes.sm : classes.md, className)} classes={{ label: classes.label }} onClick={onClick} {...subProps}>
            {icon}
        </MuiButton>) : (<MuiIconButton color="primary" variant={variant} className={clsx(size === 'sm' ? classes.sm : classes.md, className)} classes={{ root: classes.rootIcon, label: classes.label }} onClick={onClick} {...subProps}>
            {icon}
        </MuiIconButton>))
        : (<MuiButton onClick={onClick} color="primary" variant={variant} className={clsx(size === 'sm' ? classes.sm : classes.md, className)} classes={{ label: classes.label }} {...subProps}>
            {startIcon}{label}{endIcon}
        </MuiButton>)

    return tooltip ? (
        <MuiTooltip arrow title={tooltip}>
            {button}
        </MuiTooltip>
    ) : (
        button
    )
}

Button.propTypes = {
    tooltip: string,
    size: oneOf(['sm', 'md']),
    icon: element,
    variant: oneOf(['contained', 'outlined']),
    iconBorder: bool,
    onClick: func.isRequired,
    label: node,
    endIcon: element,
    startIcon: element,
}

Button.defaultProps = {
    tooltip: '',
    size: 'sm',
    icon: null,
    label: null,
    endIcon: null,
    startIcon: null,
    variant: 'contained',
    iconBorder: false,
    onClick: () => { },
}

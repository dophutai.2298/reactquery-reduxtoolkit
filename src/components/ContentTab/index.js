import React from 'react'
import {
    Line,
    ResponsiveContainer,
    ComposedChart,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from 'recharts';
import { arrayOf, node, number, object, shape, string } from 'prop-types'


const ContentTab = ({ width, height, data,label, items }) => {
    return (
        <div style={{ width: width, height: height }}>
            <ResponsiveContainer>
                <ComposedChart
                    data={data}
                    margin={{
                        top: 20, right: 20, bottom: 20, left: 20,
                    }}
                >
                    <CartesianGrid stroke="#f5f5f5" />
                    {/* interval={0} show all label */}
                    {width > 700 ? (  <XAxis dataKey={label} style={{ fontSize: '10px' }} angle={-45} textAnchor="end" interval={0}/>):
                    ( <XAxis dataKey={label} style={{ fontSize: '10px' }} angle={-45} textAnchor="end"/>)}
                  
                    <YAxis />
                    <Tooltip contentStyle={{opacity:'0.6',background:'#222'}}  labelStyle={{fontSize:'12px',color:'#fff'}} itemStyle={{fontSize:'12px',display:'block'}} />
                    <Legend wrapperStyle={{ fontFamily: 'Roboto, sans-serif', fontSize: '14px', padding: '20px' }}
                        verticalAlign='bottom'
                    />
                    {items.map((item,index)=> <Line key={item.dataKey,`-`,index} dot={false} name={item.name} connectNulls type="monotone" dataKey={item.dataKey} stroke={item.color} />)}
                </ComposedChart>
            </ResponsiveContainer>
        </div>
    )
}

ContentTab.propTypes = {
    width: number,
    height: number,
    label:node,
    data: arrayOf(object),
    items: arrayOf([
        shape({
            name:node.isRequired,
            color: node.isRequired,
            dataKey: node.isRequired
        }),
    ]).isRequired
}

ContentTab.defaultProps = {
    width: 500,
    height: 300
}


export default ContentTab
{/* {items?.length > 0 &&
                        items?.map((item) => <LineComponent color={item.color} dataKey={item.dataKey} />)} */}
export const idRegex = /[^a-zA-Z0-9_-]/g
export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}

export const autoGenId = (labels = [] || '', componentName = null) => {
  let array = []
  if (componentName && ((Array.isArray(labels) && labels.length === 0) || !labels)) {
    return `${componentName}_${uuidv4()}`
  } else {
    array = Array.isArray(labels) && labels.length > 0 ? [...labels] : [labels]
  }
  !!componentName && array.unshift(componentName)
  const labelsMerge = array.join('_')
  return labelsMerge && labelsMerge.replace(idRegex, '_').toLowerCase()
}

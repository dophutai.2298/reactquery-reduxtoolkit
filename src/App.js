
import React from 'react'
import { Backdrop, createMuiTheme, Grid, Snackbar } from '@material-ui/core';
import { useMemo } from 'react';
import themes from './components/Themes'
import Navbar from './components/Screen/Navbar'
import People from './components/Screen/People';
import Planets from './components/Screen/Planets';
import Todos from './components/Screen/Todos';
import DetailRow from './components/Screen/DetailRow';
import { ThemeProvider } from '@material-ui/styles';
import {
  BrowserRouter ,
  Route,
  Switch
} from "react-router-dom";
import Detail from './components/Screen/Detail';
import Chart from './components/Screen/Chart';
import TableScreen from './components/Screen/TableScreen';

function App() {
  const getTheme = useMemo(() => {
    return createMuiTheme({
      ...themes,
      palette: {
        ...themes.palette,
        // type: theme === THEMES.LIGHT ? 'light' : 'dark',
        type: 'light',
      },
    })
  }, [])
  return (
    <ThemeProvider theme={getTheme}>
    <BrowserRouter>
    <Grid container>
    <Grid item xs={1}></Grid>
    <Grid item xs={10}>
    <Navbar/>
      <Switch>
      <Route exact path="/planets" component={Planets} />
      <Route exact path="/todos" component={Todos} />
      <Route exact path="/people" component={People} />
      <Route exact path="/table/:id" component={DetailRow} />
      <Route exact  path="/detail/:id" component={Detail} />
      <Route exact path="/chart" component={Chart} />
      <Route exact path="/table" component={TableScreen} />
      </Switch>
    </Grid>
    <Grid item xs={1}></Grid>
   

    </Grid>
   
    
    </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;

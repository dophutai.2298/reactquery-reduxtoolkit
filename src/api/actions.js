import {productApi, serverApi} from "./product"

export const fetchProduct=()=>{
       return productApi.getProduct().then(res=>res.data).catch(err=>console.log(err))
}

export const fetchDetail=(id)=>{
    return productApi.getDetail(id).then(res=>res.data).catch(err=>console.log(err))
}

export const updateProduct=(id)=>{
    return productApi.updateProduct(id).then(res=>res.data).catch(err=>console.log(err))
}

export const removeProduct=(id)=>{
    return productApi.deleteProduct(id).then(res=>res.data).catch(err=>console.log(err))
}

export const fetchServer=()=>{
    return serverApi.getServer().then(res=>res.data).catch(err=>console.log(err))
}

export const fetchMetric=(server_name)=>{
    return serverApi.getMetric(server_name).then(res=>res.data).catch(err=>console.log(err ))
}   
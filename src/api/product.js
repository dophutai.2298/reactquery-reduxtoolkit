import Axios from 'axios'

const API = "https://5fa04305e21bab0016dfd001.mockapi.io/api/v1/listphone";

export const productApi= {
  getProduct:()=> {
    return Axios.get(API);
  },

  getDetail:(id)=> {
    return Axios.get(`${API}/${id}`);   
  },

  addProduct:(item)=> {
    return Axios.post(API, item);
  },

  deleteProduct:(id)=> {
    return Axios.delete(`${API}/${id}`);
  },

  updateProduct:(id, item)=> {
    return Axios.put(`${API}/${id}`, item);
  },
}

export const serverApi={
  getServer:()=>{
    return Axios.get('/server')
  },
  getMetric:(server_name)=>{
    return Axios.get(`/get_supported_metric?server_name=${server_name}`)
  }
}